package golang_libs

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

var dbConnProperties map[string]string

func SetDatabaseConnectionProperties(dbConnProps map[string]string) {
	dbConnProperties = dbConnProps
}

func SqlExecutor(query string, args ...interface{}) (int64, error) {
	db := GetDatabaseConnection()
	defer db.Close()

	stmt, err := db.Prepare(query)
	if err != nil {
		return 0, err
	}

	res, err := stmt.Exec(args...)
	if err != nil {
		return 0, err
	}

	return res.RowsAffected()
}

func GetDatabaseConnection() *sql.DB {
	username := dbConnProperties["username"]
	password := dbConnProperties["password"]
	host := dbConnProperties["host"]
	dbName := dbConnProperties["dbName"]

	dbConnection, err := sql.Open("mysql", prepareDataSource(username, password, host, dbName))
	if err != nil {
		panic(err)
	}

	return dbConnection
}

func prepareDataSource(username string, password string, host string, dbName string) string {
	return username + ":" + password + "@tcp(" + host + ")/" + dbName + "?charset=utf8"
}
