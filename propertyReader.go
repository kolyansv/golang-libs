package golang_libs

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

/*
  Read file line by line and parse key-values.
  Return map of key:values
*/
func ReadProperties(filePath string) map[string]string {
	properties := make(map[string]string)

	file, err := os.Open(filePath)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	counter := 0
	reader := bufio.NewReader(file)
	for {
		line, _, err := reader.ReadLine()
		// print error in case we can't read line
		if err != nil {
			fmt.Println("WARNING ::: Line can't be read. Details:", err)
		}
		// end file reading
		if line == nil {
			break
		}

		// read key:value pair
		keyValue := string(line)

		// ignore comments and invalid key-value pairs
		if len(keyValue) == 0 ||
			keyValue[0] == '#' ||
			keyValue[0] == '=' ||
			!strings.Contains(keyValue, "=") {
			continue
		}

		delimiterIndex := strings.Index(keyValue, "=")
		key := keyValue[0:delimiterIndex]
		value := keyValue[delimiterIndex+1:]

		properties[key] = value
		counter++
	}

	fmt.Printf("INFO ::: Read '%d' properties\n", counter)

	return properties
}
